import  {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import  {Observable} from "rxjs/Observable";
import  'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/fromPromise'

@Injectable()

export class HttpService{
    constructor (private  http:Http) {}

    headers() {
        let headers = new Headers();
        return new RequestOptions({headers: headers})

    };

    handleError(err) {

    };

    get (url, options = this.headers() ) {
        return Observable.fromPromise(this.http.get(url,options)
            .map( (res) => res.json())
            .toPromise().catch(err => {
                    this.handleError(err);
            }))
    }

}