import {Injectable} from "@angular/core";
import  {Constants} from "../Utils/constants"
import {HttpService} from "./http.service"

@Injectable()
export class TmpService {
    constructor(private _httpService:HttpService) {}

    getHerbs() {
        return this._httpService.get(Constants.HERBS);
    }

    getDiseases() {
        return this._httpService.get(Constants.DISEASES);
    }
}