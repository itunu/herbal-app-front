import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RecipeDetailPage} from "../recipe-detail/recipe-detail";
import  {Constants} from "../../Utils/constants";

@IonicPage()
@Component({
    selector: 'page-disease-detail',
    templateUrl: 'disease-detail.html',
})
export class DiseaseDetailPage {
    disease;
    constants = Constants.UPLOADS;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
       this.disease = this.navParams.data.disease;
        console.log(this.disease);

    }

    viewRecipe(herb,part) {
        this.navCtrl.push(RecipeDetailPage, {
            herb:herb,
            part:part
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad HerbDetailPage');
    }

}
