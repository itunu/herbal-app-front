import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HerbDetailPage } from './herb-detail';

@NgModule({
  declarations: [
    HerbDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(HerbDetailPage),
  ],
})
export class HerbDetailPageModule {}
