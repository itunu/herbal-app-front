import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HerbPartsPage} from "../herb-parts/herb-parts";
import  {Constants} from "../../Utils/constants"

/**
 * Generated class for the HerbDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-herb-detail',
  templateUrl: 'herb-detail.html',
})
export class HerbDetailPage {
    herb;
    languages  = {
      1:"YORUBA",
      2:"IGBO",
      3:"HAUSA"
    };
    constants = Constants;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.herb = this.navParams.data.herb;
      this.herb.local_name = (typeof this.herb.local_name === 'string') ? JSON.parse(this.herb.local_name) : this.herb.local_name;
  }

    viewPart(event, part) {
        this.navCtrl.push(HerbPartsPage, {
            part: part
        });
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HerbDetailPage');
  }

}
