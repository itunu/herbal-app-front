import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import  {Constants} from "../../Utils/constants"

/**
 * Generated class for the RecipeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recipe-detail',
  templateUrl: 'recipe-detail.html',
})
export class RecipeDetailPage {
 herb;
 part = [];
constants =  Constants.UPLOADS;
languages  = {
    1:"YORUBA",
    2:"IGBO",
    3:"HAUSA"
};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.herb = this.navParams.data.herb;
    this.part = this.herb.part.filter(function (p) {
       return p.recipe
    });
    this.part.forEach(function (p) {
       p.recipe[0].description =  (typeof p.recipe[0].description === 'string') ? JSON.parse(p.recipe[0].description) : p.recipe[0].description;
       p.recipe[0].description.Preparation =   (typeof p.recipe[0].description.Preparation === 'string') ? p.recipe[0].description.Preparation.split('.'): p.recipe[0].description.Preparation;
       p.recipe[0].description.Taboo =   (typeof p.recipe[0].description.Taboo === 'string') ? p.recipe[0].description.Taboo.split('.'): p.recipe[0].description.Taboo;
    });
      this.herb.local_name = (typeof this.herb.local_name === 'string') ? JSON.parse(this.herb.local_name) : this.herb.local_name;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecipeDetailPage');
  }

}
