import { Component ,OnInit} from '@angular/core';
import {LoadingController, NavController} from 'ionic-angular';
import {TmpService} from "../../services/tmp.service";
import {HerbDetailPage} from "../herb-detail/herb-detail";
import  {Constants} from "../../Utils/constants"

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
    herbs;
    constants =  Constants.UPLOADS;
    loading;

  ngOnInit() {
    this.getHerbs();
  }

  constructor(public navCtrl: NavController, public tmpService:TmpService ,public loadingCtrl: LoadingController) {

  }

  getHerbs() {
      this.showLoader();
      this.tmpService.getHerbs()
          .subscribe((res) => {
          if( res && res.data) {
              this.dismissLoader();
              this.herbs = res.data.response;
          }
      });
  }

  viewHerb(event, herb) {
      this.navCtrl.push(HerbDetailPage, {
          herb: herb
      });
  }

    showLoader() {
        this.loading = this.loadingCtrl.create({
            showBackdrop: false,
        });

        this.loading.present();
    }


    dismissLoader() {
        this.loading.dismiss();
    }

}
