import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HerbPartsPage } from './herb-parts';

@NgModule({
  declarations: [
    HerbPartsPage,
  ],
  imports: [
    IonicPageModule.forChild(HerbPartsPage),
  ],
})
export class HerbPartsPageModule {}
