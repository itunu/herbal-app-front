import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import  {Constants} from "../../Utils/constants"
/**
 * Generated class for the HerbPartsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-herb-parts',
  templateUrl: 'herb-parts.html',
})
export class HerbPartsPage {

  part;
  constants = Constants;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.part = this.navParams.data.part;
      console.log(this.part);
      this.part.disease.forEach(function (p) {
          p.recipe[0].description =  (typeof p.recipe[0].description === 'string') ? JSON.parse(p.recipe[0].description) : p.recipe[0].description;
          p.recipe[0].description.Preparation =   (typeof p.recipe[0].description.Preparation === 'string') ? p.recipe[0].description.Preparation.split('.'): p.recipe[0].description.Preparation;
          p.recipe[0].description.Taboo =   (typeof p.recipe[0].description.Taboo === 'string') ? p.recipe[0].description.Taboo.split('.'): p.recipe[0].description.Taboo;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HerbPartsPage');
  }

}
