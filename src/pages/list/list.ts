import {Component, OnInit} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {TmpService} from "../../services/tmp.service";
import {DiseaseDetailPage} from "../disease-detail/disease-detail";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage implements OnInit{
 diseases;
 searchInput= '';
 loading;

  constructor(public navCtrl: NavController, public navParams: NavParams,public tmpService:TmpService, public loadingCtrl: LoadingController) {}

  ngOnInit() {
    this.getDiseases();
  }

  getDiseases() {
      this.showLoader();
      this.tmpService.getDiseases()
          .subscribe((res) => {
          if( res && res.data) {
              this.dismissLoader();
              this.diseases = res.data.response;
          }
          });
  }

  viewDetail(disease) {
      this.navCtrl.push(DiseaseDetailPage,{
          disease:disease
      })
  }

    showLoader() {
        this.loading = this.loadingCtrl.create({
            showBackdrop: false,
        });

        this.loading.present();
    }


    dismissLoader() {
        this.loading.dismiss();
    }
}
