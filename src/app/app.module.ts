import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {HttpModule} from "@angular/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {HerbDetailPage} from "../pages/herb-detail/herb-detail"
import {HerbPartsPage} from  "../pages/herb-parts/herb-parts"
import {DiseaseDetailPage} from "../pages/disease-detail/disease-detail";
import {RecipeDetailPage} from "../pages/recipe-detail/recipe-detail"
import {DiseaseFilterPipe} from "../pipes/disease-filter/disease-filter";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {HttpService} from "../services/http.service"
import {TmpService} from "../services/tmp.service";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    HerbDetailPage,
    HerbPartsPage,
    DiseaseDetailPage,
    RecipeDetailPage,
    DiseaseFilterPipe,
  ],
  imports: [
    BrowserModule,
      HttpModule,
      FormsModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    HerbDetailPage,
    HerbPartsPage,
    DiseaseDetailPage,
    RecipeDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpService,
    TmpService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
