let API_BASE_URL = "https://vast-tor-59629.herokuapp.com/public/api";
let HERBS = API_BASE_URL + "/herbs";
let DISEASES = API_BASE_URL + "/diseases";
let UPLOADS = API_BASE_URL + "/uploads/";


export const Constants = {
   "API_BASE_URL":API_BASE_URL,
    "HERBS": HERBS,
    "DISEASES": DISEASES,
    "UPLOADS":UPLOADS
};