import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the DiseaseFilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'diseaseFilter',
})
export class DiseaseFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any,input:string) {
      console.log(value);
      console.log(input);
      if(input) {
          console.log('found.log ');
          console.log(input);
        input = input.toLowerCase();
      return value.filter(function (e) {
          return e.label.toLowerCase().indexOf(input) > - 1;
      })
    }
    return value;
  }
}
