import { NgModule } from '@angular/core';
import { DiseaseFilterPipe } from './disease-filter/disease-filter';
@NgModule({
	declarations: [DiseaseFilterPipe],
	imports: [],
	exports: [DiseaseFilterPipe]
})
export class PipesModule {}
